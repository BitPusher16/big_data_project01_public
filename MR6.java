package org.myorg;
import java.io.IOException;
import java.util.*;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.util.*;

public class MR6 {

  public static class Reduce 
    extends MapReduceBase implements Reducer<Text, Text, Text, Text> 
  {
    public void reduce(Text key, Iterator<Text> values, 
      OutputCollector<Text, Text> output, Reporter reporter) throws IOException 
    {
      // we want to remove duplicates,
      // so output only one key-value pair;
      if(values.hasNext()){
        output.collect(key, new Text(values.next().toString()));
      }
    }
  }

  public static void main(String[] args) throws Exception {
    JobConf conf = new JobConf(MR6.class);
    conf.setJobName("Distinct");

    FileInputFormat.setInputPaths(conf, new Path(args[0]));
    FileOutputFormat.setOutputPath(conf, new Path(args[1]));

    conf.setInputFormat(KeyValueTextInputFormat.class); // input has K-V format;
    conf.setOutputFormat(TextOutputFormat.class);

    conf.setOutputKeyClass(Text.class);
    conf.setOutputValueClass(Text.class);

    //conf.setMapperClass(Map.class);       // use an identity mapper;
    //conf.setCombinerClass(Reduce.class);
    conf.setReducerClass(Reduce.class);

    JobClient.runJob(conf);
  }
}





