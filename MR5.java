package org.myorg;
import java.io.IOException;
import java.util.*;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.util.*;

public class MR5 {

  public static class Reduce 
    extends MapReduceBase implements Reducer<Text, Text, Text, Text> 
  {
    public void reduce(Text key, Iterator<Text> values, 
      OutputCollector<Text, Text> output, Reporter reporter) throws IOException 
    {

      String ValueRecord = new String();
      String MovieTitle = new String();
      String UserID = new String();
      String[] elements = new String[2];
      List<String> outer = new ArrayList<String>();
      List<String> inner = new ArrayList<String>();

      while(values.hasNext()){
        String line = values.next().toString();
        elements = line.toString().split("::");
        if(elements[0].equals("UsersRatingsTable")){
          outer.add(line);
        }
        else if(elements[0].equals("MoviesTable")){
          inner.add(line);
        }
      }

      int outer_size = outer.size();
      int inner_size = inner.size();
      for(int i = 0; i < outer_size; i++){
        for(int j = 0; j < inner_size; j++){
            elements = inner.get(j).split("::");
            MovieTitle = elements[1];
            ValueRecord = "MoviesUsersRatingsTable";
            output.collect(new Text(MovieTitle), new Text(ValueRecord));
        }
      }
    }
  }

  public static void main(String[] args) throws Exception {
    JobConf conf = new JobConf(MR5.class);
    conf.setJobName("MoviesUsersRatings");

    FileInputFormat.setInputPaths(conf, new Path(args[0]));
    FileInputFormat.addInputPath(conf, new Path(args[1]));
    FileOutputFormat.setOutputPath(conf, new Path(args[2]));

    conf.setInputFormat(KeyValueTextInputFormat.class); // input has K-V format;
    conf.setOutputFormat(TextOutputFormat.class);

    conf.setOutputKeyClass(Text.class);
    conf.setOutputValueClass(Text.class);

    //conf.setMapperClass(Map.class);       // use an identity mapper;
    //conf.setCombinerClass(Reduce.class);
    conf.setReducerClass(Reduce.class);

    JobClient.runJob(conf);
  }
}




