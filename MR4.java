package org.myorg;
import java.io.IOException;
import java.util.*;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.util.*;

public class MR4 {

  public static class Map 
    extends MapReduceBase implements Mapper<LongWritable, Text, Text, Text> 
  {
    private Text ValueRecord = new Text();
    private Text uid = new Text();

    public void map(LongWritable key, Text value, 
      OutputCollector<Text, Text> output, Reporter reporter) throws IOException 
    {
      String line = value.toString();
      String[] elements = line.split("::");

      ValueRecord.set("MoviesTable::" + elements[1]);
      uid.set(elements[0]);
      output.collect(uid, ValueRecord);
    }
  }

  public static void main(String[] args) throws Exception {
    JobConf conf = new JobConf(MR4.class);
    conf.setJobName("Movies");

    FileInputFormat.setInputPaths(conf, new Path(args[0]));
    FileOutputFormat.setOutputPath(conf, new Path(args[1]));

    conf.setInputFormat(TextInputFormat.class);
    conf.setOutputFormat(TextOutputFormat.class);

    conf.setOutputKeyClass(Text.class);
    conf.setOutputValueClass(Text.class);

    conf.setMapperClass(Map.class);
    //conf.setCombinerClass(Reduce.class);
    //conf.setReducerClass(Reduce.class);
    conf.setNumReduceTasks(0);

    JobClient.runJob(conf);
  }
}



