package org.myorg;
import java.io.IOException;
import java.util.*;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.util.*;

public class MR1 {

  public static class Map 
    // mapper input key is always type LongWritable;
    // by looking at our input file, we know mapper value input is type Text;
    // we want mapper output key to be type Text;
    // we want mapper output value to be type Text;
    extends MapReduceBase implements Mapper<LongWritable, Text, Text, Text> 
  {

    private Text value_record = new Text();
    private Text user_id = new Text();

    public void map(LongWritable key, Text value, 
      OutputCollector<Text, Text> output, Reporter reporter) throws IOException 
    {
      String line = value.toString();
      String[] elements = line.split("::");

      value_record.set("UsersTable");
      user_id.set(elements[0]);
      if(elements[3].equals("12")){
          output.collect(user_id, value_record);
      }
    }
  }

  public static void main(String[] args) throws Exception {

    JobConf conf = new JobConf(MR1.class);
    conf.setJobName("Users");

    // which HDFS file should we read?
    // which HDFS file should we write to?
    FileInputFormat.setInputPaths(conf, new Path(args[0]));
    FileOutputFormat.setOutputPath(conf, new Path(args[1]));

    // file input/output format could be one of:
    // SequenceFileInputFormat, TextInputFormat, KeyValueTextInputFormat, etc.
    conf.setInputFormat(TextInputFormat.class);
    conf.setOutputFormat(TextOutputFormat.class);

    conf.setOutputKeyClass(Text.class);
    conf.setOutputValueClass(Text.class);

    conf.setMapperClass(Map.class);
    //conf.setCombinerClass(Reduce.class); // we don't need a combiner;
    //conf.setReducerClass(Reduce.class);  // we don't need a reducer;
    conf.setNumReduceTasks(0); // disable shuffle phase, reduce phase;

    JobClient.runJob(conf);
  }
}


