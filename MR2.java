package org.myorg;

import java.io.IOException;
import java.util.*;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.util.*;

public class MR2 {

  public static class Map 
    extends MapReduceBase implements Mapper<LongWritable, Text, Text, Text> 
  {

    private Text value_record = new Text();
    private Text uid = new Text();

    public void map(LongWritable key, Text value, 
      OutputCollector<Text, Text> output, Reporter reporter) throws IOException
    {
      String line = value.toString();
      String[] elements = line.split("::");

      value_record.set("RatingsTable::" + elements[1]);
      uid.set(elements[0]);
      if(Integer.parseInt(elements[2]) >= 3){
          output.collect(uid, value_record);
      }
    }
  }

  public static void main(String[] args) throws Exception {

    JobConf conf = new JobConf(MR2.class);
    conf.setJobName("Ratings");

    FileInputFormat.setInputPaths(conf, new Path(args[0]));
    FileOutputFormat.setOutputPath(conf, new Path(args[1]));

    conf.setInputFormat(TextInputFormat.class);
    conf.setOutputFormat(TextOutputFormat.class);

    conf.setOutputKeyClass(Text.class);
    conf.setOutputValueClass(Text.class);

    conf.setMapperClass(Map.class);
    //conf.setCombinerClass(Reduce.class);
    //conf.setReducerClass(Reduce.class);
    conf.setNumReduceTasks(0);

    JobClient.runJob(conf);
  }
}


